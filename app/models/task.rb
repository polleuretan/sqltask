class Task < ActiveRecord::Base
  belongs_to :project
  STATUSES = %w(created in_progress completed on_hold)
end
