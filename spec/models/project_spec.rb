require 'spec_helper'

describe Project do
  describe 'task2, task3' do
    before :each do
      #task without project
      create(:task, :project => nil)

      @expected_projects = []
      #project with 0 tasks
      project0 = create(:project, :name => '0project')
      @expected_projects.push(project0)

      #project with 1 task
      project1 = create(:project, :name => '1project')
      @expected_projects.push(project1)
      create(:task, :project => project1)

      #project with 2 tasks
      project2 = create(:project, :name => '2project')
      @expected_projects.push(project2)
      2.times do
        create(:task, :project => project2)
      end

      #project with 3 tasks
      project3 = create(:project, :name => '3project')
      @expected_projects.push(project3)
      3.times do
        create(:task, :project => project3)
      end
    end

    it '(task2)get the count of all tasks in each project, order by tasks count descending' do
      projects = Project.select('projects.*, count(tasks.id) as tasks_count')
        .joins('LEFT OUTER JOIN tasks ON projects.id=tasks.project_id')
        .group('projects.id')
        .order('tasks_count DESC').to_a

      #check order
      projects.map(&:id).should eql(@expected_projects.reverse.map(&:id))
      #check count
      @expected_projects.each_with_index do |expected_project, index|
        project = projects.find { |p| p.id == expected_project.id }
        project.tasks_count.should eql(index)
      end
    end

    it '(task3)get the count of all tasks in each project, order by projects names' do
      projects = Project.select('projects.*, count(tasks.id) as tasks_count')
        .joins('LEFT OUTER JOIN tasks ON projects.id=tasks.project_id')
        .group('projects.id')
        .order('projects.name ASC').to_a

      #check order
      projects.map(&:name).should eql(@expected_projects.map(&:name).sort)
      #check count
      @expected_projects.each_with_index do |expected_project, index|
        project = projects.find { |p| p.id == expected_project.id }
        project.tasks_count.should eql(index)
      end
    end
  end
  it '(task5)get the list of all projects containing the ‘a’ letter in the middle of the name, 
    and show the tasks count near each project. Mention that there can exist projects without 
    tasks and tasks with project_id=NULL' do

    #task without project
    create(:task, :project => nil)
    #project with letter 'a' and 2 tasks
    first_project = create(:project, :name => 'With a letter')
    #project with letter 'a' and 0 tasks
    second_project = create(:project, :name => 'Letter a also')
    #project without letter 'a' and 2 tasks
    other_project = create(:project, :name => 'No _ letter')
    2.times do
      create(:task, :project => first_project)
      create(:task, :project => other_project)
    end

    projects = Project.select('projects.*, count(tasks.id) as tasks_count')
      .where('projects.name LIKE ?', '%a%')
      .joins('LEFT OUTER JOIN tasks ON projects.id=tasks.project_id')
      .group('projects.id')

    projects.to_a.count.should eql(2)
    projects.find { |p| p.id == first_project.id }.tasks_count.should eql(2)
    projects.find { |p| p.id == second_project.id }.tasks_count.should eql(0)
  end

  it 'get the list of project names having more than 10 tasks in status ‘completed’. Order by project_id' do
    expected_projects = []
    #project with 11 tasks but with other status
    first_project = create(:project)
    #project with 11 tasks
    second_project = create(:project)
    expected_projects.push(second_project)
    #project with 11 tasks
    third_project = create(:project)
    expected_projects.push(third_project)
    11.times do
      create(:task, :project => first_project, :status => 'in_progress')
      create(:task, :project => second_project)
      create(:task, :project => third_project)
    end

    projects = Project.select('projects.*, count(tasks.id) as tasks_count')
      .joins('LEFT OUTER JOIN tasks ON projects.id=tasks.project_id')
      .where({:tasks => { :status => 'completed' }})
      .group('projects.id')
      .order('projects.id ASC')

    projects.to_a.count.should eql(2)
    projects.map(&:id).should eql(expected_projects.map(&:id).sort)
  end
end
