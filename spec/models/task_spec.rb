require 'spec_helper'

describe Task do
  it '(task1)get all statuses, not repeating, alphabetically ordered' do
    #2 tasks for each status
    2.times do
      Task::STATUSES.each do |status|
        create(:task, :status => status)
      end
    end

    tasks = Task.select(:status).distinct.order(:status)

    tasks.map(&:status).should eql(Task::STATUSES.sort)
  end

  it '(task4)get the tasks for all projects having the name beginning with “N” letter' do
    expected_tasks = [];
    unexpected_tasks = [];
    first_project = create(:project, :name => 'Nfirst')
    second_project = create(:project, :name => 'NNfirst')
    other_project = create(:project, :name => 'Other')
    2.times do
      expected_tasks.push(create(:task, :project => first_project))
      expected_tasks.push(create(:task, :project => second_project))
      unexpected_tasks.push(create(:task, :project => other_project))
    end

    tasks = Task.joins(:project).where("projects.name LIKE ?", 'N%')

    tasks.count.should eql(expected_tasks.count)
    tasks.to_a.should eql(expected_tasks)
  end

  it '(task6)get the list of tasks with duplicate names. Order alphabetically' do
    names = ['gamma', 'beta', 'alpha']
    2.times do
      #task with some other name
      create(:task)
      names.each do |name|
        #task with duplicate name
        create(:task, :name => name)
      end
    end

    tasks = Task.select('id, name, count(id) as duplicates_count')
      .group(:name)
      .having('duplicates_count > 1')
      .order('name ASC')

    tasks.map(&:name).should eql(names.sort)
  end

  it '(task7)get the list of tasks having several exact matches of both name and status, 
    from the project ‘Garage’. Order by matches count' do
    #task without project
    create(:task)
    names = ['matches1', 'matches2', 'matches3']
    project = create(:project, :name => 'Garage')
    #task with 1 match
    create(:task, :project => project)
    create(:task, :project => project, :name => names[0], :status => names[0])
    2.times do
      #tasks with 2 matches
      create(:task, :project => project, :name => names[1], :status => names[1])
    end
    3.times do
      #tasks with 3 matches
      create(:task, :project => project, :name => names[2], :status => names[2])
    end

    tasks = Task.select('tasks.*, count(*) as duplicates_count')
      .joins(:project)
      .where('projects.name = ? AND tasks.name=tasks.status', 'Garage')
      .group(:name)
      .having('duplicates_count > 1')
      .order('duplicates_count ASC')

    tasks.map(&:name).should eql([names[1], names[2]])
  end
end
