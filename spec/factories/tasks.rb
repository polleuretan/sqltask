FactoryGirl.define do
  factory :task do |f|
    project
    f.sequence(:name) { |n| "Task#{n}" }
    status "completed"
  end
end
